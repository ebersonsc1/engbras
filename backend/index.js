const express       = require('express');
const bodyparser    = require('body-parser');

var app = express();

var urlencodedParser = bodyparser.urlencoded({ extended : false });

app.use(bodyparser.json());

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.post('/', urlencodedParser, function (req, res) {
    res.send(req.body);
});

app.listen(3000);